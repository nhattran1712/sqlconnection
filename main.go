package main

import (
    "database/sql"
    _ "github.com/go-sql-driver/mysql"
	"log"
)

type Student struct {
    firstname string
    lastname string
    email string
}

func main() {
    db, err := sql.Open("mysql", "root:37673691310@tcp(127.0.0.1:3306)/golangmysql")

    if err != nil {
        panic(err.Error())
    }

    defer db.Close()

    insert, err := db.Query("INSERT INTO student(firstname, lastname, email) VALUES ('Nhat', 'Tran', 'nhat.tran@gmail.com')")

    if err != nil {
        panic(err.Error())
    }
    
	insert, err = db.Query("INSERT INTO student(firstname, lastname, email) VALUES ('Trung', 'Huynh', 'trung.huynh@gmail.com')")

    if err != nil {
        panic(err.Error())
    }

	insert, err = db.Query("INSERT INTO student(firstname, lastname, email) VALUES ('Thuy', 'Tran', 'thuy.tran@gmail.com')")

    if err != nil {
        panic(err.Error())
    }

    defer insert.Close()

    results, err := db.Query("SELECT * FROM student")

    if err != nil {
        panic(err.Error())
    }

    for results.Next() {
        var student Student
        
        err = results.Scan(&student.firstname, &student.lastname, &student.email)
        
		if err != nil {
            panic(err.Error())
        }
        
        log.Printf("%s %s %s", student.firstname, student.lastname, student.email)
    }

}